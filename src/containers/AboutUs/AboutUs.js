import React, {Component} from 'react';
import img1 from '../../assets/images/1.png'
import img2 from '../../assets/images/2.png'
import img3 from '../../assets/images/3.png'
import img4 from '../../assets/images/4.png'
import img5 from '../../assets/images/5.png'
import img6 from '../../assets/images/6.jpg'

import'./AboutUs.css';

class AboutUs extends Component {
    render() {
        return (
            <div className="About-Us">
                <h3>Hello</h3>
                <p>We are a family of seven tattoo artists: Evgeniy, Emil, Galia, Christina, Ilya and Dasha.
                    Our main purpose is to give people some art. Trust us and you will get a good tattoo!</p>
                <div className="about-photos">
                    <img src={img6} alt="img"/>
                    <img src={img1} alt="img"/>
                    <img src={img2} alt="img"/>
                    <img src={img3} alt="img"/>
                    <img src={img4} alt="img"/>
                    <img src={img5} alt="img"/>
                </div>

            </div>
        );
    }
}

export default AboutUs;