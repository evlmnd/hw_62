import React, {Component} from 'react';
import Nav from "../../components/Nav/Nav";

import './MainPage.css';

class MainPage extends Component {

    clickBookingInfo = () => {
        this.props.history.push({
            pathname: '/bookinginfo'
        })
    };

    clickContact = () => {
        this.props.history.push({
            pathname: '/contact'
        })
    };

    clickAboutUs = () => {
        this.props.history.push({
            pathname: '/aboutus'
        })
    };

    clickPortfolio = () => {
        this.props.history.push({
            pathname: '/portfolio'
        })
    };

    render() {
        return (
            <div className="Main-Page">
                <Nav
                    clickBookingInfo={this.clickBookingInfo}
                    clickContact={this.clickContact}
                    clickAboutUs={this.clickAboutUs}
                    clickPortfolio={this.clickPortfolio}
                />
                <h1>Welcome to '20/13'</h1>
            </div>
        );
    }
}

export default MainPage;