import React, {Component} from 'react';

import './BookingInfo.css';

class BookingInfo extends Component {
    render() {
        return (
            <div className="Booking-Info">
                <h3>How to book a session?</h3>
                <p>First, you should decide what exact tattoo you want to get.
                Then you choose the artist that is the most close for the style you want.
                (We can help if you have any difficulties! :) After you decide with artist, contact him/her
                and give the full information about your idea: what should be imaged in your tattoo, the size, the placement,
                 and send any pictures that can describe your preferences or references.</p>
                <p>After you give all the information the artist will book a day and time for you. Usually
                 it doesn't take a lot of time to draw a sketch for tattoos so the artist prepares
                 some sketches to your session, you discuss everything, make changes if needs and start tattooing.</p>
                <p>If you need to speak before session, artist make an appointment to consult you with all questions.</p>
            </div>
        );
    }
}

export default BookingInfo;