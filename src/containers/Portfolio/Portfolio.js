import React, {Component} from 'react';
import img1 from'../../assets/images/portfolio/1.png';
import img2 from'../../assets/images/portfolio/2.jpg';
import img3 from'../../assets/images/portfolio/3.PNG';
import img4 from'../../assets/images/portfolio/4.JPG';
import img5 from'../../assets/images/portfolio/5.jpg';
import img6 from'../../assets/images/portfolio/6.png';
import img7 from'../../assets/images/portfolio/17.PNG';
import img8 from'../../assets/images/portfolio/8.jpg';
import img9 from'../../assets/images/portfolio/9.png';
import img10 from'../../assets/images/portfolio/10.jpg';
import img11 from'../../assets/images/portfolio/11.PNG';
import img12 from'../../assets/images/portfolio/12.jpg';
import img13 from'../../assets/images/portfolio/13.png';
import img14 from'../../assets/images/portfolio/14.jpg';
import img15 from'../../assets/images/portfolio/15.png';
import img16 from'../../assets/images/portfolio/16.jpg';
import img17 from'../../assets/images/portfolio/17.PNG';
import img18 from'../../assets/images/portfolio/18.jpg';
import img19 from'../../assets/images/portfolio/19.PNG';
import img20 from'../../assets/images/portfolio/20.jpg';
import img21 from'../../assets/images/portfolio/21.png';

import './Portfolio.css';

class Portfolio extends Component {
    render() {
        return (
            <div className="Portfolio">
                <h3>Have a look on our works!</h3>
                <div className="Portfolio-photos">
                    <img src={img1} alt="img"/>
                    <img src={img2} alt="img"/>
                    <img src={img3} alt="img"/>
                    <img src={img4} alt="img"/>
                    <img src={img5} alt="img"/>
                    <img src={img6} alt="img"/>
                    <img src={img7} alt="img"/>
                    <img src={img8} alt="img"/>
                    <img src={img9} alt="img"/>
                    <img src={img10} alt="img"/>
                    <img src={img12} alt="img"/>
                    <img src={img13} alt="img"/>
                    <img src={img14} alt="img"/>
                    <img src={img15} alt="img"/>
                    <img src={img16} alt="img"/>
                    <img src={img17} alt="img"/>
                    <img src={img18} alt="img"/>
                    <img src={img19} alt="img"/>
                    <img src={img20} alt="img"/>
                    <img src={img21} alt="img"/>
                </div>
            </div>
        );
    }
}

export default Portfolio;