import React, {Component} from 'react';

import './Contact.css';

class Contact extends Component {
    render() {
        return (
            <div className="Contact">
                <h3>Our contacts</h3>
                <p>Main social media: <a href="https://www.instagram.com/2013tattoo">Instagram</a></p>
                <p>Also you can find all artists' profiles through this link and contact them via Direct Messages. This is the best way to contact
                    us so we won't give you any other information haha</p>
                <p>Also we give you our address only after you book the session. Good luck!</p>
            </div>
        );
    }
}

export default Contact;