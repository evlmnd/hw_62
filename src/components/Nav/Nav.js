import React, {Component} from 'react';

import './Nav.css';

class Nav extends Component {

    render() {
        return (
            <div className="Nav">
                <ul>
                    <li onClick={this.props.clickBookingInfo}>Booking Info</li>
                    <li onClick={this.props.clickContact}>Contact</li>
                    <li onClick={this.props.clickAboutUs}>About Us</li>
                    <li onClick={this.props.clickPortfolio}>Portfolio</li>
                </ul>
            </div>
        );
    }
}

export default Nav;