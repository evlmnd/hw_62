import React, { Component } from 'react';
import MainPage from "./containers/MainPage/MainPage";
// import Nav from './components/Nav/Nav';
import BookingInfo from './containers/BookingInfo/BookingInfo';
import AboutUs from "./containers/AboutUs/AboutUs";
import Contact from "./containers/Contact/Contact";
import Portfolio from "./containers/Portfolio/Portfolio";
import {BrowserRouter, Switch, Route} from 'react-router-dom';

import './App.css';

class App extends Component {
  render() {
    return (
        <BrowserRouter>
            <Switch>
                <Route path="/" exact component={MainPage}/>
                <Route path="/bookinginfo" exact component={BookingInfo}/>
                <Route path="/aboutus" exact component={AboutUs}/>
                <Route path="/contact" exact component={Contact}/>
                <Route path="/portfolio" exact component={Portfolio}/>
            </Switch>
        </BrowserRouter>
    );
  }
}

export default App;
